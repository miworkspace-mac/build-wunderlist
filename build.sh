#!/bin/bash -ex

# CONFIG
prefix="Wunderlist"
suffix=""
munki_package_name="Wunderlist"
display_name="Wunderlist"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o Wunderlist* -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

mv Wunderlist* App.zip

# unzip
unzip App.zip

# make DMG from the inner prefpane
mkdir app_tmp
mv Wunderlist* app_tmp
hdiutil create -srcfolder app_tmp -format UDZO -o app.dmg

# Build pkginfo
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# TODO SET THIS and remove the exit -1
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES
# echo 'TODO - configure for unattended install, if it can be'
# exit 42

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
